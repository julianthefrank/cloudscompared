# DataBase As A Service
Here we will compare the DBaaS offering from the three biggies.  
We will be looking at Like-For-Like Services offered on Managed Service basis and not the IaaS Images.
In all these products, the CSP takes care of the VM and OS Management. Even the Infrastructure Configuration of the Database is made available to Customer via a mediating Interface/UI.

```mermaid
graph TD;
    DBAAS   --> SQL;
    SQL     --> Azure;
    Azure   --> ManagedSQL(Managed SQL)
    DBAAS   --> NoSQL;
    DBAAS   --> Key-Value;
    DBAAS   --> Graph;
    DBAAS   --> Time-Series;
    
    click ManagedSQL "http://google.com" "Managed SQL"
```


## SQL
### Managed SQL DB
Cloud|Product|TL;DR|Purchase Options|CAP Modalities|Deployment Types|Data|AutoScale|Availability Options| DB Tuning|Supported Dev Tools
---|---|---|---|---|---|---|---|---|---|---
Azure|[Azure SQL](https://docs.microsoft.com/en-us/azure/sql-database/sql-database-technical-overview)|SQL Database is a general-purpose relational database managed service in Microsoft Azure that supports structures such as relational data, JSON, spatial, and XML.| 1) vCore-based purchasing model 2) DTU-based purchasing model |columnstore indexes for extreme analytic analysis and reporting, and in-memory OLTP for extreme transactional processing|1)Single database with its own set of resources managed via a SQL Database server. A single database is similar to a contained databases in SQL Server, 2)Elastic pool, which is a collection of databases with a shared set of resources managed via a SQL Database server. Single databases can be moved into and out of an elastic pool. 3)Managed instance, which is a collection of system and user databases with a shared set of resources. A managed instance is similar to an instance of the Microsoft SQL Server database engine.| 1) Single DB - Max 100TB, | 1) Single DB - No,Only Manual, 2) Elastic Pools - Yes, Basic(5 eDTU per DB, Scalewidth -> 100 - 1200 eDTUs), Standard (100 eDTU per DB, ScaleWidth -> 100 - 1200 eDTUs), Premium (1000 eDTU per DB, Scalewidth -> 125 - 1500 eDTUs)| 1) Always-On Avaialablity Groups under cover for Premium only, 2) Automatic Backup, 3) Point-in-Time Restores, 4) Active Geo Replication (upto 4 secondary DBs), 5) Auto Failover Groups, 6) Zone Redundant DBs.| Automatic Tuning -> Index Management & Plan Correction, Adaptive Query Processing | Azure Portal, SQL Server Management Studio, SQL Server Data Tools in Visual Studio, Visual Studio Code
Azure|[Azure Database for MySQL](https://azure.microsoft.com/en-us/services/mysql/)|fully managed, enterprise-ready community MySQL database as a service|---|---|---|---|---|---|---|---

## NoSQL
## Key-Value
## Graph
## Time-Series

---
## Last Updated 6th June,2019