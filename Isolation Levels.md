# Logical Isolation Levels

## Why a Ready Reckoner for Isolation
When you have your infrastructure in a private datacenter or a hosted facility, it is clearly known as to what the locations are and how the whitespace is designed in terms of Redundancy across Network, Siesmic Zones, Power Supply, Management Controller etc. When the workload moves to Public Cloud services, the level of isolation parameters an architect can ~~play~~design with start getting limited and stratified. This is good as this is as per the best practices that we have learnt over many decades and over the years CSPs themselves seem to ultimately settle down with a reasonably common Isolation design. There however are lots of differences in each layer and hence I beleive this document will help any Cloud Architect to quickly get the Isolation design related info from this document. I beleive this will save a lot of time for Cloud Architects when it comes to the Isolation design. 

## ok!ok! Lets Compare

### Fixed Configurations
The Service to Zone mapping is fixed by the CSP in this list. The Customer can choose which combination/mapping to use for their purpose.

|Blast Radius|Architect Perspective|Microsoft Azure|Amazon Web Services|Google Cloud|
|---|---|:-|:-|:-|
|Global|These services are Global in nature. Typically the entire Infrastructure for these services are located in the Main Billing location but the actual locations is never revealed by any vendor. DR/BCP is completely handled by the CSP and customers will not have any control on their design|[azure->global-infrastructure/services](https://azure.microsoft.com/en-in/global-infrastructure/services/?regions=non-regional&products=all) Called as Non-Regional Services. Eg. Azure Bot Service, Bing Services etc...|[aws->services](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/?p=ngi&loc=2#Services) Due to the nature of the service, some AWS services are delivered globally rather than regionally, such as Amazon Route 53, Amazon Chime, Amazon WorkDocs, Amazon WorkMail, Amazon WorkSpaces, Amazon WorkLink|[gcloud->global-products](https://cloud.google.com/about/locations/?region=americas#global-products) Eg. Cloud CDN,DNS, StackDriver|
|Geography Level|A geography is a discrete market, typically containing two or more regions, that preserves data residency and compliance boundaries. This however is more logical and meant to expose the design for data mobility for the purpose of DR/BCP only as part of the service offering|[azure->global-infrastructure/geographies](https://azure.microsoft.com/en-in/global-infrastructure/geographies/) 18 Options [azure->paired-regions](https://docs.microsoft.com/en-us/azure/best-practices-availability-paired-regions) Paired regions are created using Azure Traffic Manager to distribute Internet traffic to different regions, protecting an application against a regional outage. Each Azure region is paired with another region. Together, these regions form a regional pair. To meet data residency requirements for tax and law enforcement jurisdiction purposes, regional pairs are located within the same geography (with the exception of Brazil South).|[aws->Improving_Continuity_With_Replication_Between_Regions](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/?p=ngi&loc=2#Improving_Continuity_With_Replication_Between_Regions) You can also choose to increase redundancy and fault tolerance further by replicating data between geographic Regions. You can do so using both private, high speed networking and public internet connections to provide an additional layer of business continuity, or to provide low latency access across the globe.|[gcloud->multi-region](https://cloud.google.com/about/locations/?region=multi-region#region) asia,eu,us|
|Region Level|This is the highest level of Isolation afforded to customers. A Region is a set of datacenters deployed within a latency-defined perimeter and connected through a dedicated regional low-latency network. Regions are usually focused on pivotal country/city and other countries are serviced via edge infrastructure which are basically network PoP locations. The PoP locations enable the CSP to commit tight QoS. Architects should ensure that the country where these services are provided from comply with the local Data Protection and Mobility Legislation. |[azure->global-infrastructure/regions](https://azure.microsoft.com/en-in/global-infrastructure/regions/)   44-54 regions serving 140 countries.|[aws->about-aws/global-infrastructure](https://aws.amazon.com/about-aws/global-infrastructure/)   21 regions serving 19 countries|[gcloud->about/locations](https://cloud.google.com/about/locations/)   20 regions serving 200 countries!|

### Flexible Configurations
The Service to Zone mapping is configurable by the Customer. There are two types of services 
* Zonal services
  * You pin the resource to a specific zone (for example, virtual machines, managed disks, IP addresses)
* Zone-redundant services
  * CSP Platform replicates automatically across zones (for example, zone-redundant storage, SQL Database).

#### Azure
* Availability Zones
  * [azure->availability-zones](https://docs.microsoft.com/en-us/azure/availability-zones/az-overview)
  * AZs are physically separate zones within an Azure region. Each Availability Zone has a distinct power source, network, and cooling. Deploying VMs across Availability Zones helps to protect an application against datacenter-wide failures. 
  * Not all regions support Availability Zones. 
* Availability Sets
  * Availability Sets protect against localized hardware failures, such as a disk or network switch failing. VMs in an availability set are distributed across up to three fault domains. 
  * A fault domains defines a group of VMs that share a common power source and network switch. 
* Azure Site Recovery 
  * ASR replicates Azure Virtual Machines to another Azure region for business continuity (BC) and disaster recovery (DR) needs.

#### AWS 
* Availability Zones
  * [aws->Availability_Zones](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/?p=ngi&loc=2#Availability_Zones) 
  * AWS maintains 64 AZs around the world and continue to add at a fast pace. 
  * Each AZ can be multiple data centers (typically 3), and at full scale can be hundreds of thousands of servers. 
  * They are fully isolated partitions of the AWS Global Infrastructure. With their own power infrastructure, the AZs are physically separated by a meaningful distance, many kilometers, from any other AZ, although all are within 100 km (60 miles of each other)
* Placement Groups
  * [aws->placement-groups](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/placement-groups.html)
  * You can use placement groups to influence the placement of a group of interdependent instances to meet the needs of your workload. Depending on the type of workload, you can create a placement group using one of the following placement strategies:
    * Cluster
      * Packs instances close together inside an Availability Zone. This strategy enables workloads to achieve the low-latency network performance necessary for tightly-coupled node-to-node communication that is typical of HPC applications.
    * Partition
      * Spreads your instances across logical partitions such that groups of instances in one partition do not share the underlying hardware with groups of instances in different partitions. This strategy is typically used by large distributed and replicated workloads, such as Hadoop, Cassandra, and Kafka.
    * Spread
      * Strictly places a small group of instances across distinct underlying hardware to reduce correlated failures.
  
#### GCloud
* Zone 
  * [gcloud->zonal_resources](https://cloud.google.com/docs/geography-and-regions#zonal_resources) Zonal resources operate within a single zone. If a zone becomes unavailable all of the zonal resources in that zone are unavailable until service is restored. An example of a zonal resource is a Google Compute Engine instance that resides within a specific zone.


---
#### Last Updated -> 5th June,2019
